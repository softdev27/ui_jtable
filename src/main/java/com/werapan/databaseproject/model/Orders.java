/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.werapan.databaseproject.model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author ADMIN
 */
public class Orders {
    private int id;
    private Date date;
    private int qty;
    private double total;
    private ArrayList<OrderDetail> orderDetail;

    public Orders(int id, Date date, int qty, double total, ArrayList<OrderDetail> orderDetail) {
        this.id = id;
        this.date = date;
        this.qty = qty;
        this.total = total;
        this.orderDetail = orderDetail;
    }
    

   

    public Orders() {
         this.id = -1;
         orderDetail=new ArrayList<>();
         qty = 0;
         total=0;
         date=new Date();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public int getQty() {
        return qty;
    }

    public void setQty(int qty) {
        this.qty = qty;
    }

    public double getTotal() {
        return total;
    }

    public void setTotal(double total) {
        this.total = total;
    }

    public ArrayList<OrderDetail> getOrderDetail() {
        return orderDetail;
    }

    public void setOrderDetail(ArrayList<OrderDetail> orderDetail) {
        this.orderDetail = orderDetail;
    }

    @Override
    public String toString() {
        return "Orders{" + "id=" + id + ", date=" + date + ", qty=" + qty + ", total=" + total + '}';
    }

   public void addOrderDetail(OrderDetail orderDetail){
       this.orderDetail.add(orderDetail);
       total = total + orderDetail.getTotal();
       qty = qty + orderDetail.getQty();
   }
   public void addOrderDetail(Product product,String productName,double productPrice,int qty){
     OrderDetail orderDetail  = new OrderDetail(product, productName, productPrice, qty, this);
     this.addOrderDetail(orderDetail);
   }
    public void addOrderDetail(Product product,int qty){
     OrderDetail orderDetail  = new OrderDetail(product, product.getName(), product.getPrice(), qty, this);
     this.addOrderDetail(orderDetail);
   }

    public static Orders fromRS(ResultSet rs) {
        Orders orders = new Orders();
        try {
            orders.setId(rs.getInt("order_id"));
            orders.setQty(rs.getInt("orders_qty"));
            orders.setTotal(rs.getDouble("orders_total"));
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            orders.setDate(sdf.parse(rs.getString("order_date")));
        } catch (SQLException ex) {
            Logger.getLogger(User.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        } catch (ParseException ex) {
            Logger.getLogger(Orders.class.getName()).log(Level.SEVERE, null, ex);
        }
        return orders;
    }
}

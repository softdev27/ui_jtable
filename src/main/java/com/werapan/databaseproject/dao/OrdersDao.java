/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.werapan.databaseproject.dao;

import com.werapan.databaseproject.helper.DatabaseHelper;
import com.werapan.databaseproject.model.OrderDetail;
import com.werapan.databaseproject.model.Orders;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author werapan
 */
public class OrdersDao implements Dao<Orders> {

    @Override
    public Orders get(int id) {
        OrderDetailDao orderDetailDao = new OrderDetailDao();
        Orders orders = null;
        String sql = "SELECT * FROM orders WHERE orders_id=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                orders = Orders.fromRS(rs);
                List<OrderDetail> orderDetail = orderDetailDao.getByOrderId(orders.getId());
                orders.setOrderDetail((ArrayList<OrderDetail>) orderDetail);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return orders;
    }

   

    public List<Orders> getAll() {
        ArrayList<Orders> list = new ArrayList();
        String sql = "SELECT * FROM orders";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                Orders item = Orders.fromRS(rs);
                list.add(item);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }
    
    @Override
    public List<Orders> getAll(String where, String orders) {
        ArrayList<Orders> list = new ArrayList();
        String sql = "SELECT * FROM orders where " + where + " ORDER BY" + orders;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                Orders item = Orders.fromRS(rs);
                list.add(item);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }
    

    public List<Orders> getAll(String orders) {
        ArrayList<Orders> list = new ArrayList();
        String sql = "SELECT * FROM orders  ORDER BY" + orders;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                Orders item = Orders.fromRS(rs);
                list.add(item);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    @Override
    public Orders save(Orders obj) {
        OrderDetailDao orderDetailDao = new OrderDetailDao();
        String sql = "INSERT INTO orders (orders_total, orders_qty, orders_date)"
                + "VALUES(?, ?, ?)";
        Connection conn = DatabaseHelper.getConnect();
        DatabaseHelper.beginTransection();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setDouble(1, obj.getTotal());
            stmt.setInt(2, obj.getQty());
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            stmt.setString(3, sdf.format(obj.getDate()));
//            System.out.println(stmt);
            stmt.executeUpdate();
            int id = DatabaseHelper.getInsertedId(stmt);
            // Add Detail
            obj.setId(id);
            for(OrderDetail od: obj.getOrderDetail()){
                OrderDetail detail = orderDetailDao.save(od);
                if(detail == null){
                    DatabaseHelper.endTransectionWithRollback();
                    return null;
                }
            }
            DatabaseHelper.endTransectionWithCommit();
            return get(id);
        } catch (SQLException ex) {
            DatabaseHelper.endTransectionWithRollback();
            System.out.println(ex.getMessage());
            return null;
        }
        
    }

    @Override
    public Orders update(Orders obj) {
//        String sql = "UPDATE orders"
//                + " SET orders_login = ?, orders_name = ?, orders_gender = ?, orders_password = ?, orders_role = ?"
//                + " WHERE orders_id = ?";
//        Connection conn = DatabaseHelper.getConnect();
//        try {
//            PreparedStatement stmt = conn.prepareStatement(sql);
//            stmt.setString(1, obj.getLogin());
//            stmt.setString(2, obj.getName());
//            stmt.setString(3, obj.getGender());
//            stmt.setString(4, obj.getPassword());
//            stmt.setInt(5, obj.getRole());
//            stmt.setInt(6, obj.getId());
////            System.out.println(stmt);
//            int ret = stmt.executeUpdate();
//            System.out.println(ret);
//            return obj;
//        } catch (SQLException ex) {
//            System.out.println(ex.getMessage());
            return null;
//        }
    }

    @Override
    public int delete(Orders obj) {
        String sql = "DELETE FROM orders WHERE orders_id=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getId());
            int ret = stmt.executeUpdate();
            return ret;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return -1;        
    }

}

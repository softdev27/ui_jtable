/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package poc;

import com.werapan.databaseproject.dao.OrdersDao;
import com.werapan.databaseproject.model.OrderDetail;
import com.werapan.databaseproject.model.Orders;
import com.werapan.databaseproject.model.Product;
import java.util.ArrayList;

/**
 *
 * @author ADMIN
 */
public class TestOrder {
    public static void main(String[] args) {
        Product product1 = new Product(1, "A", 75);
        Product product2 = new Product(2, "B", 180);
        Product product3 = new Product(3, "C", 70);
        Orders order =new Orders();
//        OrderDetail orderDetail1 = new OrderDetail(product1, product1.getName(), product1.getPrice(), 1, order);
//        order.addOrderDetail(orderDetail1);
        order.addOrderDetail(product1, 1);
        order.addOrderDetail(product2, 1);
        order.addOrderDetail(product3, 1);
        System.out.println(order);
        System.out.println(order.getOrderDetail());
        printReciept(order);
        
        OrdersDao ordersDao = new OrdersDao();
        Orders newOrders = ordersDao.save(order);
        System.out.println(newOrders);

        System.out.println(ordersDao.get(3));
    }
   static void printReciept(Orders order){
        System.out.println("Order" + order.getId());
        for (OrderDetail od: order.getOrderDetail()){
            System.out.println(" " + od.getProductName()+" "+od.getQty()+" "+od.getProductPrice()+" "+od.getTotal());
        }
        System.out.println("Total : " + order.getTotal()+"Qty : "+order.getQty());
    }

}
